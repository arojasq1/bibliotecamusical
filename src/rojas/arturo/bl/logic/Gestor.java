package rojas.arturo.bl.logic;

import rojas.arturo.bl.entities.admin.*;
import rojas.arturo.bl.entities.album.Album;
import rojas.arturo.bl.entities.album.AlbumDAO;
import rojas.arturo.bl.entities.artist.Artist;
import rojas.arturo.bl.entities.artist.ArtistDAO;
import rojas.arturo.bl.entities.composer.Composer;
import rojas.arturo.bl.entities.composer.ComposerDAO;
import rojas.arturo.bl.entities.genre.Genre;
import rojas.arturo.bl.entities.genre.GenreDAO;
import rojas.arturo.bl.entities.song.Song;
import rojas.arturo.bl.entities.song.SongDAO;

import java.time.LocalDate;
import java.util.ArrayList;

public class Gestor {
    public String[] getAdmins() throws Exception {
        AdminDAO dao = new AdminDAO();
        ArrayList<Admin> admins = dao.listarAdmins();
        String[] info = new String[admins.size()];
        int pos = 0;
        for (Admin a : admins) {
            info[pos] = a.toString();
            pos++;
        }
        return info;
    }

    public void registrarAdmin(String nombre, String apellidoUno, String apellidoDos, String email, String username, String pass, String foto) throws Exception {
        AdminDAO dao = new AdminDAO();
        Admin a = new Admin(nombre, apellidoUno, apellidoDos, email, username, pass, foto);//crea la instancia del Admin
        dao.registrarAdmin(a); //lo manda a guardar a la base de datos.
    }

    public boolean login(String username, String pass) throws Exception {
        boolean login = false;
        AdminDAO dao = new AdminDAO();
        ArrayList<Admin> admins = dao.listarAdmins();
        for (Admin a : admins) {
            if (a.getUsername().equals(username) && a.getPass().equals(pass)) {
                login = true;
                break;
            }
        }
        return login;
    }

    public void registrarGenre(String nombre, String descripcion) throws Exception {
        GenreDAO dao = new GenreDAO();
        Genre g = new Genre(nombre, descripcion);
        dao.registrarGenre(g);
    }

    public void registrarComposer(String nombre, String apellido, String paisNacimiento, int age) throws Exception {
        ComposerDAO dao = new ComposerDAO();
        Composer c = new Composer(nombre, apellido, paisNacimiento, age);
        dao.registrarComposer(c);
    }

    public void registrarAlbum(String name, String cover, LocalDate lanzamiento, Artist artistaAlbum) throws Exception {
        AlbumDAO dao = new AlbumDAO();
        Album a = new Album(name, cover, lanzamiento, artistaAlbum);
        dao.registrarAlbum(a);
    }

    public void registrarArtist(String nombre, String apellido, String alias, String paisNacimiento, String genero, String about,
                                int age, LocalDate nacimiento, LocalDate defuncion) throws Exception {
        ArtistDAO dao = new ArtistDAO();
        Artist a = new Artist(nombre, apellido, alias, paisNacimiento, genero, about, age, nacimiento, defuncion);
        dao.registrarArtist(a);
    }

    public void registrarSong(String name, LocalDate releaseDate, Double rating, Double price, String path, String source,
                              Composer composer, Genre genre, Album albumCancion) throws Exception {
        SongDAO dao = new SongDAO();
        Song s = new Song(name, releaseDate, rating, price, path, source, composer, genre, albumCancion);
        dao.registrarSong(s);
    }

    public void registrarSong2(String name, LocalDate releaseDate, Double rating, Double price, String path, String source,
                               String composer, String genre, String albumCancion) throws Exception {
        SongDAO dao = new SongDAO();
        Composer c = new Composer(composer, null, null, 30);
        Genre g = new Genre(genre, "lo-fi");
        Artist nujabes = new Artist("Jun", "Seba", "Nujabes", "Japon", "M", "Reconocido como el padre del lo-fi", 30,
                LocalDate.parse("1980-01-01"), LocalDate.parse("2009-01-01"));
        Album a = new Album(albumCancion, null, LocalDate.parse("2003-08-21"), nujabes);
        Song s = new Song(name, releaseDate, rating, price, path, source, c, g, a);
        dao.registrarSong(s);
    }

    public String[] listarSong() throws Exception {
        SongDAO dao = new SongDAO();
        ArrayList<Song> songs = dao.listarSongs();
        if (songs == null) {
            songs = new ArrayList<>();
        }
        String[] info = new String[songs.size()];
        int pos = 0;
        for (Song s : songs) {
            info[pos] = s.toString();
            pos++;
        }
        return info;
    }

    public Song getCancion() throws Exception {
        SongDAO dao = new SongDAO();
        Song cancion = null;
        ArrayList<Song> songs = dao.listarSong2();
        for(Song s:songs) {
            cancion = s;
        }
        return cancion;
    }
}

package rojas.arturo.bl.entities.cola;

import rojas.arturo.bl.entities.Lista;
import rojas.arturo.bl.entities.song.Song;
import rojas.arturo.bl.entities.corriente.UsuarioCorriente;

import java.util.ArrayList;

/**
 * La cola de reproducción de los usuarios
 */
public class Cola extends Lista {
    /**
     * El constructor por defecto
     */
    public Cola() {
        super();
    }

    /**
     *
     * @param id el numero de identificacion de la lista
     * @param canciones las canciones de la lista
     * @param usuario el usuario al que le pertenece la lista
     */
    public Cola(int id, ArrayList<Song> canciones, UsuarioCorriente usuario) {
        super(id, canciones, usuario);
    }

    /**
     * método que envía la información separada por comas
     * @return la información de la cola
     */
    public String toDatabase() {
        return id + "," + canciones.toString() + "," + usuario.toDatabase();
    }

    /**
     * método que imprime la información de la cola de reproducción
     * @return toda la información de la cola de reproducción
     */
    @Override
    public String toString() {
        return "Cola{" +
                "id=" + id +
                ", canciones=" + canciones +
                ", usuario=" + usuario +
                '}';
    }
}
package rojas.arturo.bl.entities.playlist;

import rojas.arturo.bl.entities.Lista;
import rojas.arturo.bl.entities.song.Song;
import rojas.arturo.bl.entities.corriente.UsuarioCorriente;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Listas de reproducción de los usuarios
 */
public class Playlist extends Lista {
    /**
     * el string con el nombre
     */
    private String name;
    /**
     * el local date con la fecha de creación del playlist
     */
    private LocalDate creacion;
    /**
     * el double con la calificación del playlist (la media de la calificación de cada canción)
     */
    private double rating;

    /**
     * El constructor por defecto
     */
    public Playlist() {
        super();
    }

    /**
     *
     * @param id el numero de identificacion de la lista
     * @param canciones las canciones de la lista
     * @param usuario el usuario al que le pertenece la lista
     * @param name el nombre de la lista
     * @param creacion la fecha de creacion
     * @param rating la calificación promedio de la lista
     */
    public Playlist(int id, ArrayList<Song> canciones, UsuarioCorriente usuario, String name, LocalDate creacion, double rating) {
        super(id, canciones, usuario);
        this.name = name;
        this.creacion = creacion;
        this.rating = rating;
    }

    /**
     * método de acceso del atributo name
     * @return el nombre de la lista de reproducción
     */
    public String getName() {
        return name;
    }

    /**
     * método de acceso del atributo name
     * @param name el nombre de la lista de reproducción
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * método de acceso del atributo creacion
     * @return la fecha de creación de la lista de reproducción
     */
    public LocalDate getCreacion() {
        return creacion;
    }

    /**
     * método de acceso del atributo creacion
     * @param creacion la fecha de creación de la lista de reproducción
     */
    public void setCreacion(LocalDate creacion) {
        this.creacion = creacion;
    }

    /**
     * método de acceso del atributo rating
     * @return la calificación del playlist (la media de la calificación de cada canción)
     */
    public double getRating() {
        return rating;
    }

    /**
     * método de acceso del atributo rating
     * @param rating la calificación del playlist (la media de la calificación de cada canción)
     */
    public void setRating(double rating) {
        this.rating = rating;
    }

    /**
     * método que envía la información separada por comas
     * @return la información de la lista de reproducción
     */
    public String toDatabase() {
        return id + "," + canciones.toString() + "," + usuario.toDatabase() + "," + name + "," + creacion + "," + rating;
    }

    /**
     * método que imprime la información de la lista de reproduccion
     * @return toda la información de la lista de reproduccion
     */
    @Override
    public String toString() {
        return "Playlist{" +
                "name='" + name + '\'' +
                ", creacion=" + creacion +
                ", rating=" + rating +
                '}';
    }
}

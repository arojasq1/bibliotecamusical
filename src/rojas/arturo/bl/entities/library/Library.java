package rojas.arturo.bl.entities.library;

import rojas.arturo.bl.entities.Lista;
import rojas.arturo.bl.entities.song.Song;
import rojas.arturo.bl.entities.corriente.UsuarioCorriente;

import java.util.ArrayList;

/**
 * La librería de canciones del usuario
 */
public class Library extends Lista {
    /**
     * El constructor por defecto
     */
    public Library() {
        super();
    }

    /**
     * El constructor por parámetros
     * @param id el numero de identificacion de la lista
     * @param canciones las canciones de la lista
     * @param usuario el usuario al que le pertenece la lista
     */
    public Library(int id, ArrayList<Song> canciones, UsuarioCorriente usuario) {
        super(id, canciones, usuario);
    }

    /**
     * método que envía la información separada por comas
     * @return la información de la libreria
     */
    public String toDatabase() {
        return id + "," + canciones.toString() + "," + usuario.toDatabase();
    }

    /**
     * método que imprime la información de la libreria
     * @return toda la información de la libreria
     */
    @Override
    public String toString() {
        return "Library{" +
                "id=" + id +
                ", canciones=" + canciones +
                ", usuario=" + usuario +
                '}';
    }
}
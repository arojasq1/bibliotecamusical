package rojas.arturo.bl.entities;

import rojas.arturo.bl.entities.song.Song;
import rojas.arturo.bl.entities.corriente.UsuarioCorriente;

import java.util.ArrayList;

public class Lista {
    /**
     * El int con el id de la lista de reproduccion
     */
    protected int id;
    /**
     * El array list que contiene las canciones de la lista de reproducción
     */
    protected ArrayList<Song> canciones;
    /**
     * El atributo tipo Usuario al que pertenece la lista de reproducción
     */
    protected UsuarioCorriente usuario;

    /**
     * Constructor por defecto
     */
    public Lista() {
    }

    /**
     * Constructor por parámetros
     * @param id el numero de identificacion de la lista
     * @param canciones las canciones de la lista
     * @param usuario el usuario al que le pertenece la lista
     */
    public Lista(int id, ArrayList<Song> canciones, UsuarioCorriente usuario) {
        this.id = id;
        this.canciones = canciones;
        this.usuario = usuario;
    }

    /**
     * el método de acceso del atributo id
     * @return el id de la lista de reproduccion
     */
    public int getId() {
        return id;
    }

    /**
     * el método de acceso del atributo id
     * @param id el id de la lista de reproduccion
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * método de acceso del atributo canciones
     * @return el arraylist con las canciones de la lista
     */
    public ArrayList<Song> getCanciones() {
        return canciones;
    }

    /**
     * método de acceso del atributo canciones
     * @param canciones el arraylist con las canciones de la lista
     */
    public void setCanciones(ArrayList<Song> canciones) {
        this.canciones = canciones;
    }

    /**
     * método de acceso del atributo usuario
     * @return el usuario al que le pertenece la lista
     */
    public UsuarioCorriente getUsuario() {
        return usuario;
    }

    /**
     * método de acceso del atributo usuario
     * @param usuario el usuario al que le pertenece la lista
     */
    public void setUsuario(UsuarioCorriente usuario) {
        this.usuario = usuario;
    }

    /**
     * método que envía la información separada por comas
     * @return la información del usuario
     */
    public String toDatabase() {
        return id + "," + canciones.toString() + "," + usuario.toDatabase();
    }

    /**
     * método que retorna la información del usuario
     * @return la información del usuario
     */
    @Override
    public String toString() {
        return "Lista{" +
                "id=" + id +
                ", canciones=" + canciones +
                ", usuario=" + usuario +
                '}';
    }

    /**
     * override del metodo equals
     * @param o el objeto a revisar
     * @return booleano que indica si el objeto ya existe o no
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lista l = (Lista) o;
        return l.equals(l.id);
    }
}

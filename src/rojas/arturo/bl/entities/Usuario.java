package rojas.arturo.bl.entities;

/**
 * Clase padre de los usuarios de la aplicación
 */
public class Usuario {
    /**
     * String con el nombre
     */
    protected String nombre;
    /**
     * String con el primer apellido
     */
    protected String apellidoUno;
    /**
     * String con el segundo apellido
     */
    protected String apellidoDos;
    /**
     * String con el correo
     */
    protected String email;
    /**
     * String con el username
     */
    protected String username;
    /**
     * String con la contraseña
     */
    protected String pass; //minimo 8 caracteres, max 12, minimo 1 minuscula, mayuscula, caracter especial
    /**
     * String con el url de la foto de perfil
     */
    protected String foto;

    /**
     * Constructor por defecto
     */
    public Usuario() {
    }

    /**
     * constructor por parámetros del usuario
     * @param nombre el string con el nombre
     * @param apellidoUno el string con el primer apellido
     * @param apellidoDos el string con el segundo apellido
     * @param email el string con el correo
     * @param username el string con el nombre
     * @param pass el string con la contraseña
     * @param foto el string con el url de la foto de perfil
     */
    public Usuario(String nombre, String apellidoUno, String apellidoDos, String email, String username, String pass, String foto) {
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.email = email;
        this.username = username;
        this.pass = pass;
        this.foto = foto;
    }

    /**
     * método de acceso del atributo nombre
     * @return el nombre del usuario
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * método de acceso del atributo nombre
     * @param nombre el nombre del usuario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * método de acceso del atributo apellido
     * @return el primer apellido del usuario
     */
    public String getApellidoUno() {
        return apellidoUno;
    }

    /**
     * método de acceso del atributo apellido
     * @param apellidoUno el primer apellido del usuario
     */
    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    /**
     * método de acceso del atributo apellidoDos
     * @return el segundo apellido del usuario
     */
    public String getApellidoDos() {
        return apellidoDos;
    }

    /**
     * método de acceso del atributo apellidoDos
     * @param apellidoDos el segundo apellido del usuario
     */
    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    /**
     * método de acceso del atributo email
     * @return el correo del usuario
     */
    public String getEmail() {
        return email;
    }

    /**
     * método de acceso del atributo email
     * @param email el correo del usuario
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * método de acceso del atributo username
     * @return el nombre de usuario
     */
    public String getUsername() {
        return username;
    }

    /**
     * método de acceso del atributo username
     * @param username el nombre de usuario
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * método de acceso del atributo pass
     * @return la contraseña del usuario
     */
    public String getPass() {
        return pass;
    }

    /**
     * método de acceso del atributo pass
     * @param pass la contraseña del usuario
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * método de acceso del atributo foto
     * @return el url con la foto del usuario
     */
    public String getFoto() {
        return foto;
    }

    /**
     * método de acceso del atributo foto
     * @param foto el url con la foto del usuario
     */
    public void setFoto(String foto) {
        this.foto = foto;
    }

    /**
     * método que envía la información separada por comas
     * @return la información del usuario
     */
    public String toDatabase() {
        return nombre + "," + apellidoUno + "," + apellidoDos + "," + email + "," + username + "," + pass  + "," + foto;
    }

    /**
     * método que retorna la información del usuario
     * @return la información del usuario
     */
    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", apellidoUno='" + apellidoUno + '\'' +
                ", apellidoDos='" + apellidoDos + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", pass='" + pass + '\'' +
                ", foto='" + foto + '\'' +
                '}';
    }

    /**
     * override del metodo equals
     * @param o el objeto a revisar
     * @return booleano que indica si el objeto ya existe o no
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario u = (Usuario) o;
        return email.equals(u.email);
    }
}

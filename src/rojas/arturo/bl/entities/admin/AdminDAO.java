package rojas.arturo.bl.entities.admin;

import rojas.arturo.accesodatos.*;
import rojas.arturo.utils.Utilities;

import java.sql.*;
import java.util.ArrayList;

public class AdminDAO {
    public ArrayList<Admin> listarAdmins() throws Exception {
        ArrayList<Admin> admins = null;
        try {
            String query = "SELECT nombre, apellidoUno, apellidoDos, email, username, pass, foto FROM admin";
            ResultSet rs = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (admins == null) {
                    admins = new ArrayList<>();
                }
                Admin admin = new Admin(rs.getString("nombre"),
                        rs.getString("apellidoUno"), rs.getString("apellidoDos"),
                        rs.getString("email"), rs.getString("username"), rs.getString("pass"), rs.getString("foto"));
                admins.add(admin);
            }
            return admins;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public void registrarAdmin(Admin admin) throws Exception{
        try {
            String query = "INSERT INTO admin (nombre, apellidoUno, apellidoDos, email, username, pass, foto)" +
                    "VALUES ('" + admin.getNombre() +"','" + admin.getApellidoUno() + "','" + admin.getApellidoDos() +
                    "','" + admin.getEmail() + "','" + admin.getUsername() + "','" + admin.getPass() + "','" + admin.getFoto() + "')";
            Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public String actualizarPass(String newPass, String oldPass, String email) throws Exception{
        String mensaje;
        try {
            String query = "UPDATE admin SET pass = '"+newPass+"' WHERE pass = '"+oldPass+"' AND email = '"+email+"'";
            Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
            mensaje = "Contraseña actualizada con éxito";
            return mensaje;
        }
        catch (Exception ex){
            throw ex;
        }
    }

}

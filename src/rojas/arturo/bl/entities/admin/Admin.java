package rojas.arturo.bl.entities.admin;
import rojas.arturo.bl.entities.Usuario;

/**
 * El usuario administrador de la aplicación
 */
public class Admin extends Usuario{
    /**
     * El constructor por defecto
     */
    public Admin() {
        super();
    }

    /**
     * El constructor por parámetros
     * @param username el username del admin
     * @param pass la contrseña del admin
     * @param nombre el nombre del admin
     * @param apellidoUno el apellido del admin
     * @param apellidoDos el segundo apellido del admin
     * @param email el correo del admin
     */
    public Admin(String nombre, String apellidoUno, String apellidoDos, String email, String username, String pass, String foto) {
        super(nombre, apellidoUno, apellidoDos, email, username, pass, foto);
    }

    /**
     * método que envía la información separada por comas
     * @return la información del usuario separada por comas
     */
    @Override
    public String toDatabase() {
        return nombre + "," + apellidoUno + "," + apellidoDos + "," + email + "," + username + "," + pass  + "," + foto;
    }

    /**
     * método que imprime la información del admin
     * @return toda la información del admin
     */
    @Override
    public String toString() {
        return "Admin{" +
                "nombre='" + nombre + '\'' +
                ", apellidoUno='" + apellidoUno + '\'' +
                ", apellidoDos='" + apellidoDos + '\'' +
                ", username='" + username + '\'' +
                ", pass='" + pass + '\'' +
                ", foto='" + foto + '\'' +
                '}';
    }
}

package rojas.arturo.bl.entities.corriente;

import rojas.arturo.bl.entities.tarjeta.Tarjeta;
import rojas.arturo.bl.entities.Usuario;

import java.util.ArrayList;

/**
 * Usuario de la aplicación
 */
public class UsuarioCorriente extends Usuario {
    /**
     * String con el pais de procedencia
     */
    private String pais; //TODO crear catálogo de paises
    /**
     * String con la identificación del usuario
     */
    private String id;
    /**
     * Entero con la edad
     */
    private int edad;
    /**
     * Entero con el token de primer ingreso del usuario
     */
    private int token;
    /**
     * Array List que almacena las tarjetas
     */
    private ArrayList<Tarjeta> tarjetasUsuario;

    /**
     * Constructor por defecto
     */
    public UsuarioCorriente() {
        super();
    }

    /**
     * constructor por parámetros del usuario
     * @param pais string con el pais de procedencia
     * @param id string con la identificación
     * @param edad enter con la edad
     * @param token el token para el FTL del usuario
     */
    public UsuarioCorriente(String nombre, String apellidoUno, String apellidoDos, String email, String username, String pass, String foto, String pais, String id, int edad, int token, ArrayList<Tarjeta> tarjetasUsuario) {
        super(nombre, apellidoUno, apellidoDos, email, username, pass, foto);
        this.pais = pais;
        this.id = id;
        this.edad = edad;
        this.token = token;
        this.tarjetasUsuario = tarjetasUsuario;
    }

    /**
     * método de acceso del atributo pais
     * @return el pais de procedencia del usuario
     */
    public String getPais() {
        return pais;
    }

    /**
     * método de acceso del atributo pais
     * @param pais el pais de procedencia del usuario
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * método de acceso del atributo id
     * @return la identificación del usuario
     */
    public String getId() {
        return id;
    }

    /**
     * método de acceso del atributo id
     * @param id la identificación del usuario
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * método de acceso del atributo edad
     * @return la edad del usuario
     */
    public int getEdad() {
        return edad;
    }

    /**
     * método de acceso del atributo edad
     * @param edad la edad del usuario
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * método de acceso del atributo token
     * @return el token de FTL del usuario
     */
    public int getToken() {
        return token;
    }

    /**
     * método de acceso del atributo token
     * @param token el token de FTL del usuario
     */
    public void setToken(int token) {
        this.token = token;
    }

    /**
     * método de accesso del array list de tarjetas
     * @return el array list con las tarjetas del usuario
     */
    public ArrayList<Tarjeta> getTarjetasUsuario() {
        return tarjetasUsuario;
    }

    /**
     * método de accesso del array list de tarjetas
     * @param tarjetasUsuario el array list con las tarjetas del usuario
     */
    public void setTarjetasUsuario(ArrayList<Tarjeta> tarjetasUsuario) {
        this.tarjetasUsuario = tarjetasUsuario;
    }

    /**
     * método que envía la información separada por comas
     * @return la información del usuario separada por comas
     */
    @Override
    public String toDatabase() {
        return nombre + "," + apellidoUno + "," + apellidoDos + "," + email + "," + username + "," + pass + "," + foto + "," + pais + "," + id + "," + edad + "," + token  + ","
                + tarjetasUsuario.toString(); //TODO revisar si el arraylist.toString esta bien
    }

    /**
     * método que imprime la información del usuario
     * @return toda la información del usuario
     */
    @Override
    public String toString() {
        return "UsuarioCorriente{" +
                "pais='" + pais + '\'' +
                ", id='" + id + '\'' +
                ", edad=" + edad +
                ", token=" + token +
                ", tarjetasUsuario=" + tarjetasUsuario +
                '}';
    }
}
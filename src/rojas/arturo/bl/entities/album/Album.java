package rojas.arturo.bl.entities.album;

import rojas.arturo.bl.entities.artist.Artist;

import java.time.LocalDate;

/**
 * Los álbumes disponibles en la aplicación
 */
public class Album {
    /**
     * El string con el nombre del álbum
     */
    private String name;
    /**
     * El string con el url de la portada del álbum
     */
    private String cover;
    /**
     * El Local date con la fecha de lanzamiento del álbum
     */
    private LocalDate lanzamiento;
    /**
     * El atributo tipo artist del artista al que pertenece la canción
     */
    private Artist artistaAlbum;

    /**
     * Constructor por defecto
     */
    public Album() {
    }

    /**
     * El constructor por parámetros
     * @param name el nombre de la canción
     * @param cover la portada del álbum
     * @param lanzamiento la fecha de lanzamiento del álbum
     * @param artistaAlbum el artista al que pertenece el álbum
     */
    public Album(String name, String cover, LocalDate lanzamiento, Artist artistaAlbum) {
        this.name = name;
        this.cover = cover;
        this.lanzamiento = lanzamiento;
        this.artistaAlbum = artistaAlbum;
    }

    /**
     * El método de acceso del atributo name
     * @return el nombre del álbum
     */
    public String getName() {
        return name;
    }

    /**
     * El método de acceso del atributo name
     * @param name el nombre del álbum
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * método de accesso del atributo cover
     * @return la portada del álbum
     */
    public String getCover() {
        return cover;
    }

    /**
     * método de accesso del atributo cover
     * @param cover la portada del álbum
     */
    public void setCover(String cover) {
        this.cover = cover;
    }

    /**
     * método de accesso del atributo lanzamiento
     * @return la fecha de lanzamiento del álbum
     */
    public LocalDate getLanzamiento() {
        return lanzamiento;
    }

    /**
     * método de accesso del atributo lanzamiento
     * @param lanzamiento la fecha de lanzamiento del álbum
     */
    public void setLanzamiento(LocalDate lanzamiento) {
        this.lanzamiento = lanzamiento;
    }

    /**
     * método de accesso del atributo artist
     * @return el artista al que pertenece el álbum
     */
    public Artist getArtistaAlbum() {
        return artistaAlbum;
    }

    /**
     * método de acceso del atributo artist
     * @param artistaAlbum el artista al que pertenece el álbum
     */
    public void setArtistaAlbum(Artist artistaAlbum) {
        this.artistaAlbum = artistaAlbum;
    }

    /**
     * método que envía la información separada por comas
     * @return la información del album separada por comas
     */
    public String toDatabase() {
        return name + "," + cover + "," + lanzamiento + "," + artistaAlbum;
    }

    /**
     * método que imprime la información del album
     * @return toda la información del album
     */
    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", cover='" + cover + '\'' +
                ", lanzamiento=" + lanzamiento +
                ", artistaAlbum=" + artistaAlbum +
                '}';
    }
}
package rojas.arturo.bl.entities.album;

import rojas.arturo.accesodatos.Connector;
import rojas.arturo.bl.entities.artist.Artist;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class AlbumDAO {

    public void registrarAlbum(Album album) throws Exception{
        try {
            Artist a = album.getArtistaAlbum();
            String artista = a.getAlias();
            String query = "INSERT INTO album (name, cover, lanzamiento, artistaAlbum)" +
                    "VALUES ('" + album.getName() +"','" + album.getCover() + "','" + album.getLanzamiento() +
                    "','" + artista + "')";
            Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public ArrayList<Album> listarAlbums() throws Exception {
        ArrayList<Album> albums = null;
        try {
            String query = "SELECT name, cover, lanzamiento, artistaAlbum FROM album";
            ResultSet rs = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (albums == null) {
                    albums = new ArrayList<>();
                }
                Album album = new Album(rs.getString("name"),
                        rs.getString("cover"), LocalDate.parse(rs.getString("lanzamiento")),
                        null);
                String query2 = "SELECT nombre,apellido,alias,paisNacimiento,genero,nacimiento FROM artista WHERE alias ='" + rs.getString("artistaAlbum") + "'";
                ResultSet rs2 = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query2);
                while (rs2.next()) {Artist a = new Artist(rs2.getString("nombre"),rs2.getString("apellido"), rs2.getString("alias"),
                        rs2.getString("paisNacimiento"),rs2.getString("genero"),rs2.getString("about"),rs2.getInt("age"),
                        LocalDate.parse(rs2.getString("nacimiento")),LocalDate.parse(rs2.getString("nacimiento")));
                album.setArtistaAlbum(a);}
                albums.add(album);
            }

            return albums;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }
}

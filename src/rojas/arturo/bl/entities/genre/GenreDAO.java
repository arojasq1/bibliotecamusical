package rojas.arturo.bl.entities.genre;

import rojas.arturo.accesodatos.Connector;
import rojas.arturo.bl.entities.genre.Genre;
import rojas.arturo.bl.entities.genre.Genre;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GenreDAO {
    
    public void registrarGenre(Genre genre) throws Exception{
        try {
            String query = "INSERT INTO genre (nombre,descripcion)" +
                    "VALUES ('" + genre.getNombre() +"','" + genre.getDescripcion() + "')";
            Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public ArrayList<Genre> listarGenres() throws Exception {
        ArrayList<Genre> genres = null;
        try {
            String query = "SELECT nombre,descripcion FROM genre";
            ResultSet rs = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (genres == null) {
                    genres = new ArrayList<>();
                }
                Genre genre = new Genre(rs.getString("nombre"),
                        rs.getString("descripcion"));
                genres.add(genre);
            }
            return genres;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

}

package rojas.arturo.bl.entities.genre;

/**
 * Los géneros musicales
 */
public class Genre {
    /**
     * El string con el nombre
     */
    private String nombre;
    /**
     * el nombre con la descripción
     */
    private String descripcion;

    /**
     * El constructor por defecto
     */
    public Genre() {
    }

    /**
     * El constructor por parámetros
     * @param nombre el nombre del género musical
     * @param descripcion la descripción
     */
    public Genre(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    /**
     * método de acceso del atributo nombre
     * @return el nombre del género musical
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * método de acceso del atributo nombre
     * @param nombre el nombre del género musical
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * método de acceso del atributo descripción
     * @return la descripción del género musical
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * método de acceso del atributo descripción
     * @param descripcion la descripción del género musical
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * método que envía la información separada por comas
     * @return la información del genero separada por comas
     */
    public String toDatabase() {
        return nombre + "," + descripcion;
    }

    /**
     * método que imprime la información del genero
     * @return toda la información del genero
     */
    @Override
    public String toString() {
        return "Genre{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}

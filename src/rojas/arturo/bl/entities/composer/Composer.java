package rojas.arturo.bl.entities.composer;

/**
 * Los compositores de canciones
 */
public class Composer {
    /**
     * El string con el nombre
     */
    private String nombre;
    /**
     * El string con el apellido
     */
    private String apellido;
    /**
     * El string con el pais de nacimiento
     */
    private String paisNacimiento;
    /**
     * el entero con la edad
     */
    private int age;

    /**
     * Constructor por defecto
     */
    public Composer() {
    }

    /**
     * El constructor por parámetros
     * @param nombre el nombre del compositor
     * @param apellido el apellido del compositor
     * @param paisNacimiento el pais de nacimiento del compositor
     * @param age la edad del compositor
     */
    public Composer(String nombre, String apellido, String paisNacimiento, int age) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.paisNacimiento = paisNacimiento;
        this.age = age;
    }

    /**
     * método de acceso del atributo nombre
     * @return el nombre del compositor
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * método de acceso del atributo nombre
     * @param nombre el nombre del compositor
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * método de acceso del atributo apellido
     * @return el apellido del compositor
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * método de acceso del atributo apellido
     * @param apellido el apellido del compositor
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * método de acceso del atributo paisNacimiento
     * @return el pais de nacimiento del compositor
     */
    public String getPaisNacimiento() {
        return paisNacimiento;
    }

    /**
     * método de acceso del atributo paisNacimiento
     * @param paisNacimiento el pais de nacimiento del compositor
     */
    public void setPaisNacimiento(String paisNacimiento) {
        this.paisNacimiento = paisNacimiento;
    }

    /**
     * método de acceso del atributo age
     * @return la edad del compositor
     */
    public int getAge() {
        return age;
    }

    /**
     * método de acceso del atributo age
     * @param age la edad del compositor
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * método que envía la información separada por comas
     * @return la información del compositor separada por comas
     */
    public String toDatabase() {
        return nombre + "," + apellido + "," + paisNacimiento + "," + age;
    }

    /**
     * método que imprime la información del compositor
     * @return toda la información del compositor
     */
    @Override
    public String toString() {
        return "Composer{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", paisNacimiento='" + paisNacimiento + '\'' +
                ", age=" + age +
                '}';
    }
}

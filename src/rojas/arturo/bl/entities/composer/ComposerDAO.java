package rojas.arturo.bl.entities.composer;

import rojas.arturo.accesodatos.Connector;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ComposerDAO {

    public void registrarComposer(Composer composer) throws Exception{
        try {
            String query = "INSERT INTO composer (nombre,apellido,paisNacimiento,age)" +
                    "VALUES ('" + composer.getNombre() +"','" + composer.getApellido() + "','" + composer.getPaisNacimiento() +
                    "','" + composer.getAge() + "')";
            Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public ArrayList<Composer> listarComposers() throws Exception {
        ArrayList<Composer> composers = null;
        try {
            String query = "SELECT nombre,apellido,paisNacimiento,age FROM composer";
            ResultSet rs = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (composers == null) {
                    composers = new ArrayList<>();
                }
                Composer composer = new Composer(rs.getString("nombre"),
                        rs.getString("apellido"), rs.getString("paisNacimiento"),
                        rs.getInt("age"));
                composers.add(composer);
            }
            return composers;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }
}

package rojas.arturo.bl.entities.artist;

import rojas.arturo.bl.entities.song.Song;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Los artistas registrados en la aplicación
 */
public class Artist {
    /**
     * El string con el nombre de pila
     */
    private String nombre;
    /**
     * El string con el apellido
     */
    private String apellido;
    /**
     * El string con el alias o el nombre de artista
     */
    private String alias;
    /**
     * El string con el pais de nacimiento
     */
    private String paisNacimiento;
    /**
     * El caracter con el género
     */
    private String genero;
    /**
     * El string con la descripción/el about
     */
    private String about;
    /**
     * El entero con la edad del artista
     */
    private int age; //derivado
    /**
     * El Local date con la fecha de nacimiento
     */
    private LocalDate nacimiento;
    /**
     * El local date con la fecha de defunción (si aplica)
     */
    private LocalDate defuncion;
    /**
     * El array list que almacena las canciones del artista
     */
    private ArrayList<Song> cancionesArtista;

    /**
     * El constructor por defecto
     */
    public Artist() {
    }

    /**
     * El constructor por parámetro
     * @param nombre el nombre del artista
     * @param apellido el apellido del artista
     * @param alias el alias del artista
     * @param paisNacimiento el pais de nacimiento del artista
     * @param genero el género del artista
     * @param nacimiento el país de nacimiento del artista
     */
    public Artist(String nombre, String apellido, String alias, String paisNacimiento, String genero, String about, int age, LocalDate nacimiento, LocalDate defuncion) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.alias = alias;
        this.paisNacimiento = paisNacimiento;
        this.genero = genero;
        this.about = about;
        this.age = age;
        this.nacimiento = nacimiento;
        this.defuncion = defuncion;
    }

    /**
     * método de acceso del atributo nombre
     * @return el nombre del artista
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * método de acceso del atributo nombre
     * @param nombre el nombre del artista
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * método de acceso del atributo apellido
     * @return el apellido del artista
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * método de acceso del atributo apellido
     * @param apellido el apellido del artista
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * método de acceso del atributo alias
     * @return el alias del artista
     */
    public String getAlias() {
        return alias;
    }

    /**
     * método de acceso del atributo alias
     * @param alias el alias del artista
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * método de acceso del atributo paisNacimiento
     * @return el pais de nacimiento del artista
     */
    public String getPaisNacimiento() {
        return paisNacimiento;
    }

    /**
     * método de acceso del atributo paisNacimiento
     * @param paisNacimiento el pais de nacimiento del artista
     */
    public void setPaisNacimiento(String paisNacimiento) {
        this.paisNacimiento = paisNacimiento;
    }

    /**
     * método de acceso del atributo genero
     * @return el género del usuario
     */
    public String getGenero() {
        return genero;
    }

    /**
     * método de acceso del atributo genero
     * @param genero el género del usuario
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * método de acceso del atributo about
     * @return la descripción/el about del artista
     */
    public String getAbout() {
        return about;
    }

    /**
     * método de acceso del atributo about
     * @param about la descripción/el about del artista
     */
    public void setAbout(String about) {
        this.about = about;
    }

    /**
     * método de acceso del atributo age
     * @return la edad del artisa
     */
    public int getAge() {
        return age;
    }

    /**
     * método de acceso del atributo age
     * @param age la edad del artisa
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * método de acceso del atributo nacimiento
     * @return la fecha de nacimiento del artista
     */
    public LocalDate getNacimiento() {
        return nacimiento;
    }

    /**
     * método de acceso del atributo nacimiento
     * @param nacimiento la fecha de nacimiento del artista
     */
    public void setNacimiento(LocalDate nacimiento) {
        this.nacimiento = nacimiento;
    }

    /**
     * método de acceso del atributo defunción
     * @return la fecha de defunción del artista
     */
    public LocalDate getDefuncion() {
        return defuncion;
    }

    /**
     * método de acceso del atributo defunción
     * @param defuncion la fecha de defunción del artista
     */
    public void setDefuncion(LocalDate defuncion) {
        this.defuncion = defuncion;
    }

    /**
     * método de acceso del array list de canciones
     * @return el array list con canciones del artista
     */
    public ArrayList<Song> getCancionesArtista() {
        return cancionesArtista;
    }

    /**
     * método de acceso del array list de canciones
     * @param cancionesArtista el array list con canciones del artista
     */
    public void setCancionesArtista(ArrayList<Song> cancionesArtista) {
        this.cancionesArtista = cancionesArtista;
    }

    /**
     * método que envía la información separada por comas
     * @return la información del artista separada por comas
     */
    public String toDatabase() {
        return nombre + "," + apellido + "," + alias + "," + paisNacimiento + "," + genero + "," + about + "," + age + "," + nacimiento + "," + defuncion + "," + cancionesArtista.toString();
    }

    /**
     * método que imprime la información del artista
     * @return toda la información de la artista
     */
    @Override
    public String toString() {
        return "Artist{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", alias='" + alias + '\'' +
                ", paisNacimiento='" + paisNacimiento + '\'' +
                ", genero=" + genero +
                ", about='" + about + '\'' +
                ", age=" + age +
                ", nacimiento=" + nacimiento +
                ", defuncion=" + defuncion +
                ", cancionesArtista=" + cancionesArtista +
                '}';
    }
}
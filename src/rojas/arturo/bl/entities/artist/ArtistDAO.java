package rojas.arturo.bl.entities.artist;

import rojas.arturo.accesodatos.Connector;
import rojas.arturo.bl.entities.artist.Artist;
import rojas.arturo.bl.entities.song.Song;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ArtistDAO {

    public void registrarArtist(Artist artist) throws Exception {
        try {
            ArrayList<Song> canciones = artist.getCancionesArtista();
            //Crea cancion_artista
            String query2 = "INSERT INTO cancion_artista (alias,cancion1,cancion2,cancion3,cancion4,cancion5)" +
                    "VALUES ('" + artist.getAlias() + "','" + null + "','" + null + "','" + null + "','" + null + "','" + null + "')";
            Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query2);

            String query = "INSERT INTO artist (nombre,apellido,alias,paisNacimiento,genero,about,age,nacimiento,defuncion)" +
                    "VALUES ('" + artist.getNombre() + "','" + artist.getApellido() + "','" + artist.getAlias() +
                    "','" + artist.getPaisNacimiento() + "','" + artist.getGenero() + "','" + artist.getAbout() + "','" + artist.getAge() + "','" + artist.getNacimiento()
                    + "','" + artist.getDefuncion() + "')";
            Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }

    }

    public ArrayList<Artist> listarArtists() throws Exception {
        ArrayList<Artist> artists = null;
        try {
            ArrayList<Song> canciones = new ArrayList<>();
            String query = "SELECT nombre,apellido,alias,paisNacimiento,genero,about,age,nacimiento,defuncion FROM artist";
            ResultSet rs = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (artists == null) {
                    artists = new ArrayList<>();
                }
                Artist artist = new Artist(rs.getString("nombre"),rs.getString("apellido"), rs.getString("alias"),
                        rs.getString("paisNacimiento"),rs.getString("genero"),rs.getString("about"),rs.getInt("age"),
                        LocalDate.parse(rs.getString("nacimiento")),LocalDate.parse(rs.getString("nacimiento")));
                artists.add(artist);
            }

            return artists;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }
}
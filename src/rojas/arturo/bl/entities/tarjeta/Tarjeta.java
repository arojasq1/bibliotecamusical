package rojas.arturo.bl.entities.tarjeta;

import java.time.LocalDate;

/**
 * Método de pago de los usuarios
 */
public class Tarjeta {
    /**
     * String con el nombre
     */
    private String name;
    /**
     * String con el apellido
     */
    private String apellidoUno;
    /**
     * String con el segundo apellido
     */
    private String apellidoDos;
    /**
     * String con el número de la tarjeta
     */
    private String number; //TODO aqui seria pertinente dejarlo como string pero crear una validacion que no permita letras. no se como
    /**
     * el int con el cvv de la tarjeta
     */
    private int cvv;
    /**
     * El local date con la fecha de expiración
     */
    private LocalDate expiracion;

    /**
     * El constructor por defecto
     */
    public Tarjeta() {
    }

    /**
     * el constructor por parámetros
     * @param name el nombre de la tarjeta
     * @param apellidoUno el apellido de la tarjeta
     * @param apellidoDos el segundo apellido de la tarjeta
     * @param number el número de la tarjeta
     * @param cvv el cvv de la tarjeta
     * @param expiracion la fecha de expiración de la tarjeta
     */
    public Tarjeta(String name, String apellidoUno, String apellidoDos, String number, int cvv, LocalDate expiracion) {
        this.name = name;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.number = number;
        this.cvv = cvv;
        this.expiracion = expiracion;
    }

    /**
     * método de acceso del atributo name
     * @return el nombre de la tarjeta
     */
    public String getName() {
        return name;
    }

    /**
     * método de acceso del atributo name
     * @param name el nombre en la tarjeta
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * método de acceso del atributo apellidoUno
     * @return el apellido en la tarjeta
     */
    public String getApellidoUno() {
        return apellidoUno;
    }

    /**
     * método de acceso del atributo apellidoUno
     * @param apellidoUno el apellido en la tarjeta
     */
    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    /**
     * método de acceso del atributo apellidoDos
     * @return el segundo apellido en la tarjeta
     */
    public String getApellidoDos() {
        return apellidoDos;
    }

    /**
     * método de acceso del atributo apellidoDos
     * @param apellidoDos el segundo apellido en la tarjeta
     */
    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    /**
     * método de acceso del atributo number
     * @return el número de la tarjeta
     */
    public String getNumber() {
        return number;
    }

    /**
     * método de acceso del atributo number
     * @param number el número de la tarjeta
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * método de acceso del atributo cvv
     * @return el cvv de la tarjeta
     */
    public int getCvv() {
        return cvv;
    }

    /**
     * método de acceso del atributo cvv
     * @param cvv el cvv de la tarjeta
     */
    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    /**
     * método de acceso del atributo expiracion
     * @return la fecha de expiración de la tarjeta
     */
    public LocalDate getExpiracion() {
        return expiracion;
    }

    /**
     * método de acceso del atributo expiracion
     * @param expiracion la fecha de expiración de la tarjeta
     */
    public void setExpiracion(LocalDate expiracion) {
        this.expiracion = expiracion;
    }

    /**
     * método que envía la información separada por comas
     * @return la información de la tarjeta
     */
    public String toDatabase() {
        return name + "," + apellidoUno + "," + apellidoDos + "," + number + "," + cvv + "," + expiracion;
    }

    /**
     * método que imprime la información de la tarjeta
     * @return toda la información de la tarjeta
     */
    @Override
    public String toString() {
        return "Tarjeta{" +
                "name='" + name + '\'' +
                ", apellidoUno='" + apellidoUno + '\'' +
                ", apellidoDos='" + apellidoDos + '\'' +
                ", number='" + number + '\'' +
                ", cvv=" + cvv +
                ", expiracion=" + expiracion +
                '}';
    }
}

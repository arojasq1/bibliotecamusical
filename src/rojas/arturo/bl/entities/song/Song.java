package rojas.arturo.bl.entities.song;
import rojas.arturo.bl.entities.album.Album;
import rojas.arturo.bl.entities.composer.Composer;
import rojas.arturo.bl.entities.genre.Genre;

import java.time.LocalDate;

/**
 * Canciones disponibles en la aplicación
 */
public class Song {
    /**
     * String con el nombre
     */
    private String name;
    /**
     * Local Date con la fecha de lanzamiento de la canción
     */
    private LocalDate releaseDate;
    /**
     * El double con la calificación de la canción
     */
    private Double rating;
    /**
     * El double con el precio de la canción
     */
    private Double price;
    /**
     * El string con la dirección física de la canción en la máquina
     */
    private String path;
    /**
     * Caracter que indica si la canción fue comprada en la tienda o subida por el usuario
     */
    private String source;
    /**
     * Atributo tipo composer que almacena el compositor de la canción
     */
    private Composer composer;
    /**
     * Atributo tipo genre que almacena el género musical de la canción
     */
    private Genre genre;
    /**
     * Atributo tipo album que almacena el álbum al que pertenece la canción
     */
    private Album albumCancion;

    /**
     * Constructor por defecto
     */
    public Song() {
    }

    /**
     * constructor por parámetros de la canción
     * @param name el nombre de la canción
     * @param releaseDate la fecha de lanzamiento de la canción
     * @param composer el compositor de la canción
     * @param genre el género musica de la canción
     * @param albumCancion el álbum al que pertenece la canción
     */
    public Song(String name, LocalDate releaseDate, Double rating, Double price, String path, String source,
                Composer composer, Genre genre, Album albumCancion) {
        this.name = name;
        this.releaseDate = releaseDate;
        this.rating = rating;
        this.price = price;
        this.path = path;
        this.source = source;
        this.composer = composer;
        this.genre = genre;
        this.albumCancion = albumCancion;
    }

    /**
     * método de accesso del atributo name
     * @return el nombre de la cancion
     */
    public String getName() {
        return name;
    }

    /**
     * método de accesso del atributo name
     * @param name el nombre de la canción
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * método de accesso del atributo releaseDate
     * @return la fecha de lanzamiento de la canción
     */
    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    /**
     * método de accesso del atributo releaseDate
     * @param releaseDate la fecha de lanzamiento de la canción
     */
    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * método de accesso del atributo rating
     * @return la calificación de la canción
     */
    public Double getRating() {
        return rating;
    }

    /**
     * método de accesso del atributo rating
     * @param rating la calificación de la canción
     */
    public void setRating(Double rating) {
        this.rating = rating;
    }

    /**
     * método de accesso del atributo price
     * @return el precio de la canción
     */
    public Double getPrice() {
        return price;
    }

    /**
     * método de accesso del atributo price
     * @param price el precio de la canción
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * método de accesso del atributo path
     * @return la ubicación física de la canción
     */
    public String getPath() {
        return path;
    }

    /**
     * método de accesso del atributo path
     * @param path la ubicación física de la canción
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * método de accesso del atributo source
     * @return el origen de la canción, comprada o subida
     */
    public String getSource() {
        return source;
    }

    /**
     * método de accesso del atributo source
     * @param source el origen de la canción, comprada o subida
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * método de accesso del atributo composer
     * @return el compositor de la canción
     */
    public Composer getComposer() {
        return composer;
    }

    /**
     * método de accesso del atributo composer
     * @param composer el compositor de la canción
     */
    public void setComposer(Composer composer) {
        this.composer = composer;
    }

    /**
     * método de accesso del atributo género
     * @return el género musical de la canción
     */
    public Genre getGenre() {
        return genre;
    }

    /**
     * método de accesso del atributo género
     * @param genre el género musical de la canción
     */
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    /**
     * método de accesso del atributo album
     * @return el álbum al que pertenece la canción
     */
    public Album getAlbumCancion() {
        return albumCancion;
    }

    /**
     * método de accesso del atributo album
     * @param albumCancion el álbum al que pertenece la canción
     */
    public void setAlbumCancion(Album albumCancion) {
        this.albumCancion = albumCancion;
    }

    /**
     * método que envía la información separada por comas
     * @return la información de la canción
     */
    public String toDatabase() {
        return name + "," + releaseDate + "," + composer.toDatabase() + "," + genre.toDatabase() + "," +albumCancion.toDatabase();
    }

    /**
     * método que imprime la información de la canción
     * @return toda la información de la canción
     */
    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", releaseDate=" + releaseDate +
                ", rating=" + rating +
                ", price=" + price +
                ", path='" + path + '\'' +
                ", source=" + source +
                ", composer=" + composer +
                ", genre=" + genre +
                ", albumCancion=" + albumCancion +
                '}';
    }
}
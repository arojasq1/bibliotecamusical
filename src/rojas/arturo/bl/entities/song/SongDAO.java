package rojas.arturo.bl.entities.song;

import rojas.arturo.accesodatos.Connector;
import rojas.arturo.bl.entities.album.Album;
import rojas.arturo.bl.entities.artist.Artist;
import rojas.arturo.bl.entities.composer.Composer;
import rojas.arturo.bl.entities.genre.Genre;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SongDAO {
    public void registrarSong(Song song) throws Exception {
        try {
            Genre genre = song.getGenre();
            String g = genre.getNombre();
            Composer composer = song.getComposer();
            String c = composer.getNombre();
            Album album = song.getAlbumCancion();
            String a = album.getName();
            String query = "INSERT INTO song (name,releaseDate,rating,price,path,source,composer,genre,albumCancion)" +
                    "VALUES ('" + song.getName() + "','" + song.getReleaseDate() + "','" + song.getRating() +
                    "','" + song.getPrice() + "','" + song.getPath() + "','" + song.getSource() + "','" + c
                    + "','" + g + "','" + a + "')";
            Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public ArrayList<Song> listarSong2() throws Exception {
        ArrayList<Song> songs = null;
        try {
            String query = "SELECT name,releaseDate,rating,price,path,source,composer,genre,albumCancion FROM song";
            ResultSet rs = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (songs == null) {
                    songs = new ArrayList<>();
                }
                Song song = new Song(rs.getString("name"),
                        LocalDate.parse(rs.getString("releaseDate")), rs.getDouble("rating"),
                        rs.getDouble("price"), rs.getString("path"), rs.getString("source"),
                        null, null, null);
                songs.add(song);
            }
            return songs;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }


    public ArrayList<Song> listarSongs() throws Exception {
        ArrayList<Song> songs = null;
        try {
            String query = "SELECT name,releaseDate,rating,price,path,source,composer,genre,albumCancion FROM song";
            ResultSet rs = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (songs == null) {
                    songs = new ArrayList<>();
                }
                Song song = new Song(rs.getString("name"),
                        LocalDate.parse(rs.getString("releaseDate")), rs.getDouble("rating"),
                        rs.getDouble("price"), rs.getString("path"), rs.getString("source"),
                        null, null, null);

                String query2 = "SELECT nombre,apellido,paisNacimiento,age FROM composer WHERE nombre ='" + rs.getString("composer") + "'";
                ResultSet rs2 = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query2);
                while (rs2.next()) {
                    Composer c = new Composer(rs2.getString("nombre"), rs2.getString("apellido"), rs2.getString("paisNacimiento"),
                            rs2.getInt("age"));
                    song.setComposer(c);
                }

                String query3 = "SELECT nombre,descripcion FROM genre WHERE nombre ='" + rs.getString("genre") + "'";
                ResultSet rs3 = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query3);
                while (rs3.next()) {
                    Genre g = new Genre(rs3.getString("nombre"), rs3.getString("descripcion"));
                    song.setGenre(g);
                }

                String query4 = "SELECT name,cover,lanzamiento,artistaAlbum FROM album WHERE name ='" + rs.getString("artistaAlbum") + "'";
                ResultSet rs4 = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query4);
                while (rs4.next()) {
                    Album a = new Album(rs2.getString("name"), rs2.getString("cover"), LocalDate.parse(rs2.getString("lanzamiento")),
                            null);

                    String query5 = "SELECT nombre,apellido,alias,paisNacimiento,genero,nacimiento FROM artista WHERE alias ='" + rs4.getString("artistaAlbum") + "'";
                    ResultSet rs5 = Connector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query5);
                    while (rs5.next()) {
                        Artist ar = new Artist(rs5.getString("nombre"), rs5.getString("apellido"), rs5.getString("alias"),
                                rs5.getString("paisNacimiento"), rs5.getString("genero"), rs5.getString("about"), rs5.getInt("age"),
                                LocalDate.parse(rs5.getString("nacimiento")), LocalDate.parse(rs5.getString("nacimiento")));
                        a.setArtistaAlbum(ar);
                    }
                    song.setAlbumCancion(a);
                }

                songs.add(song);
            }

            return songs;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }
}

package rojas.arturo.utils;

import java.io.FileInputStream;
import java.util.Properties;

public class Utilities {
    public static String[] getProperties() throws Exception {
        String [] dbinfo = new String[2];
        Properties prop = new Properties();
        prop.load(new FileInputStream("db.props"));
        dbinfo[0] = prop.getProperty("driver");
        dbinfo[1] = prop.getProperty("url");
        return dbinfo;
    }
}
